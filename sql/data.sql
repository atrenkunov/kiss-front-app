INSERT INTO team
(id, name, owner, phone)
VALUES('c121312', 'Моя команда', 'Ivanov Ivan', '8-916-123-321-12-21');


INSERT INTO repository
(id, team_id, link, name)
VALUES('repo1', 'c121312', 'http://bitbucket.ru/repo1', 'Репозиторий Java кода');
INSERT INTO repository
(id, team_id, link, name)
VALUES('repo2', 'c121312', 'http://bitbucket.ru/repo2', 'Репозиторий фронтового приложения');


INSERT INTO repository_branch (repository_id,branchId, last_commit,module_version) VALUES (
'repo1',"origin/develop",'efklewfjwef','1.0');

INSERT INTO repository_branch (repository_id,branchId, last_commit,module_version) VALUES (
'repo1',"origin/master",'dfdsfdsfsdf','0.1');

INSERT INTO jira_task (taskName, description, version, task_type, branch, lastBuildNumber, taskStatus) VALUES (
'Внедрение биометрии', 'Разработать механизмы биометрии через потоковую камеру', '0.0.1', 'task', 'feature/GKR-123', '0.0.1.125', 'IN PROGRESS')
