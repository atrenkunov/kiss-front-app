# Сборщик веток

## Получить список шагов сборки

```
GET api/executor/build/stage/all
```

OUT:

```json
[
  {
    "id": "download_sources",
    "name": "Скачиваю ветку из репозитория"
  },
  {
    "id": "compile",
    "name": "Компиляция программы"
  },
  {
    "id": "unit_tests",
    "name": "Запуск unit тестов"
  },
  {
    "id": "sonar",
    "name": "Запуск сонара"
  },
  {
    "id": "oss",
    "name": "Запуск oss"
  },
  {
    "id": "publish",
    "name": "Публикация артефактов"
  }
]
```

## Постановка ветки на сборку

POST api/executor/build

IN:

```json
{
  "teamId": "c121312",
  "repositoryId": "repo1",
  "branchId": "develop",
  "checkList": [
    "unit_tests",
    "sonar"
  ]
}
```

OUT:

```json
{
  "createTimestamp": "2021-09-30T15:27:18.504+03:00",
  "id": "2",
  "type": "BUILD",
  "repositoryId": "repo1",
  "branchId": "origin/master",
  "moduleVersion": "0.1",
  "state": "WAITING",
  "stages": [
    {
      "id": "download_sources",
      "description": null,
      "state": "WAITING",
      "progress": 0
    },
    {
      "id": "compile",
      "description": null,
      "state": "WAITING",
      "progress": 0
    },
    {
      "id": "unit_tests",
      "description": null,
      "state": "WAITING",
      "progress": 0
    },
    {
      "id": "sonar",
      "description": null,
      "state": "WAITING",
      "progress": 0
    },
    {
      "id": "publish",
      "description": null,
      "state": "WAITING",
      "progress": 0
    }
  ]
}
```

## Получение текущего статуса сборки

```
GET api/executor/build/{buildId}/status
```

OUT:

```json
{
  "createTimestamp": "2021-09-30T15:26:02.311+03:00",
  "id": "1",
  "type": "BUILD",
  "repositoryId": "repo1",
  "branchId": "develop",
  "moduleVersion": "unknown",
  "state": "COMPLETED",
  "stages": [
    {
      "id": "download_sources",
      "description": "Stage id=download_sources completed successfully",
      "state": "COMPLETED",
      "progress": 100
    },
    {
      "id": "compile",
      "description": "Stage id=compile completed successfully",
      "state": "COMPLETED",
      "progress": 100
    },
    {
      "id": "unit_tests",
      "description": "Stage id=unit_tests completed successfully",
      "state": "COMPLETED",
      "progress": 100
    },
    {
      "id": "sonar",
      "description": "Stage id=sonar completed successfully",
      "state": "COMPLETED",
      "progress": 100
    },
    {
      "id": "publish",
      "description": "Stage id=publish completed successfully",
      "state": "COMPLETED",
      "progress": 100
    }
  ]
}
```

### Получение артефакта сборки

```
GET /api/executor/build/{buildId}/result
```

OUT:

```json
{
    "id": "0455134b-5e5e-45be-9db2-0a44b38d9b33",
    "name": "0.0.1.1",
    "link": "http://nexus.ru/builds/0.0.1.1/summary",
    "timestamp": "2021-10-02T10:11:37.3169081+03:00",
    "buildInfo": {
        "createTimestamp": "2021-10-02T07:11:03.4538341Z",
        "id": "1",
        "type": "BUILD",
        "repositoryId": "repo1",
        "branchId": "origin/master",
        "commit": "dfdsf",
        "moduleVersion": "0.0.1",
        "state": "COMPLETED",
        "stages": [
            {
                "id": "unit_tests",
                "description": null,
                "state": "SKIPPED",
                "progress": 0
            },
            {
                "id": "sonar",
                "description": "Stage id=sonar completed successfully",
                "state": "COMPLETED",
                "progress": 100
            },
            {
                "id": "oss",
                "description": "Stage id=oss completed successfully",
                "state": "COMPLETED",
                "progress": 100
            }
        ]
    }
}
```

### Получение списка всех сборок команды

```
GET api/executor/build/all/byTeam?teamId={teamId}
```

OUT:

```json
[
  {
    "createTimestamp": "2021-09-30T15:26:02.311+03:00",
    "id": "1",
    "type": "BUILD",
    "repositoryId": "repo1",
    "branchId": "develop",
    "moduleVersion": "unknown",
    "state": "COMPLETED",
    "stages": [
      {
        "id": "download_sources",
        "description": "Stage id=download_sources completed successfully",
        "state": "COMPLETED",
        "progress": 100
      },
      {
        "id": "compile",
        "description": "Stage id=compile completed successfully",
        "state": "COMPLETED",
        "progress": 100
      },
      {
        "id": "unit_tests",
        "description": "Stage id=unit_tests completed successfully",
        "state": "COMPLETED",
        "progress": 100
      },
      {
        "id": "sonar",
        "description": "Stage id=sonar completed successfully",
        "state": "COMPLETED",
        "progress": 100
      },
      {
        "id": "publish",
        "description": "Stage id=publish completed successfully",
        "state": "COMPLETED",
        "progress": 100
      }
    ]
  },
  {
    "createTimestamp": "2021-09-30T15:27:18.504+03:00",
    "id": "2",
    "type": "BUILD",
    "repositoryId": "repo1",
    "branchId": "origin/master",
    "moduleVersion": "0.1",
    "state": "COMPLETED",
    "stages": [
      {
        "id": "download_sources",
        "description": "Stage id=download_sources completed successfully",
        "state": "COMPLETED",
        "progress": 100
      },
      {
        "id": "compile",
        "description": "Stage id=compile completed successfully",
        "state": "COMPLETED",
        "progress": 100
      },
      {
        "id": "unit_tests",
        "description": "Stage id=unit_tests completed successfully",
        "state": "COMPLETED",
        "progress": 100
      },
      {
        "id": "sonar",
        "description": "Stage id=sonar completed successfully",
        "state": "COMPLETED",
        "progress": 100
      },
      {
        "id": "publish",
        "description": "Stage id=publish completed successfully",
        "state": "COMPLETED",
        "progress": 100
      }
    ]
  }
]
```