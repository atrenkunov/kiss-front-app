# Установщик артефактов

## Получить список шагов установки

```
GET api/executor/install/stage/all
```

OUT:

```json
[
  {
    "id": "download",
    "name": "Скачиваю артефакт"
  },
  {
    "id": "install",
    "name": "Установка"
  },
  {
    "id": "checkup",
    "name": "Проверка установки"
  }
]
```