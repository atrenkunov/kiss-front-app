# Создание машины

```
POST /api/machine
```

IN
```json
{
	"ip": "1.2.3.4",
	"name": "mach 1",
	"type": "IFT",
	"teamId": "c7"
}

```

# Удаление машины по id

```
DELETE machine/{machineId}
```

# Установка версии на машину

```
POST /api/machine/{machineId}/install
```

IN 
```json
{
	"owner": "Anton",
	"description": "Test GKR-123",
	"artifactId": "fcc44747-6e13-4cf5-b0d2-b4dde936673b",
	"machineId": "str1"
}
```


OUT
```json
{
   "id": "str1",
   "ip": "192.168.80.1",
   "name": "Windows Machine",
   "type": "DEV",
   "installedArtifact":    {
      "id": "fcc44747-6e13-4cf5-b0d2-b4dde936673b",
      "name": "0.0.2.4",
      "link": "http://nexus.ru/builds/0.0.2.4/summary",
      "timestamp": "2021-10-05T19:15:02.496+03:00",
      "buildInfo":       {
         "createTimestamp": "2021-10-05T16:14:45.181Z",
         "id": "4",
         "type": "BUILD",
         "repositoryId": "repo1",
         "branchId": "origin/develop",
         "commit": "sasacwe",
         "moduleVersion": "0.0.2",
         "state": "COMPLETED",
         "stages":          [
                        {
               "id": "unit_tests",
               "description": null,
               "state": "SKIPPED",
               "progress": 0
            },
                        {
               "id": "sonar",
               "description": null,
               "state": "SKIPPED",
               "progress": 0
            },
                        {
               "id": "oss",
               "description": null,
               "state": "SKIPPED",
               "progress": 0
            }
         ]
      }
   },
   "installationProgress":    {
      "createTimestamp": "2021-10-05T19:15:37.19+03:00",
      "id": "5",
      "type": "INSTALL",
      "state": "WAITING",
      "stages":       [
                  {
            "id": "download_sources",
            "description": null,
            "state": "WAITING",
            "progress": 0
         },
                  {
            "id": "install",
            "description": null,
            "state": "WAITING",
            "progress": 0
         },
                  {
            "id": "checkup",
            "description": null,
            "state": "WAITING",
            "progress": 0
         }
      ]
   },
   "jiraTask": null,
   "owner": "Anton",
   "description": "Test GKR-123",
   "jiraTaskId": null
}
```


# Получение списка стендов

```
GET api/machine/{machineSegment}/byTeam?teamId={teamId}  machineType=DEV,IFT,ST,PSI,PROM
```

```json
[
  {
    "id": "str1",
    "ip": "192.168.80.1",
    "name": "Windows Machine",
    "type": "DEV",
    "installedArtifact":       {
      "id": "94b574d2-d658-42b3-abf1-1bd48de6dbfe",
      "name": "0.0.2.1",
      "link": "http://nexus.ru/builds/0.0.2.1/summary",
      "timestamp": "2021-10-05T19:08:47.176+03:00",
      "buildInfo":          {
        "createTimestamp": "2021-10-05T16:08:29.843Z",
        "id": "1",
        "type": "BUILD",
        "repositoryId": "repo1",
        "branchId": "origin/develop",
        "commit": "sasacwe",
        "moduleVersion": "0.0.2",
        "state": "COMPLETED",
        "stages":             [
          {
            "id": "unit_tests",
            "description": null,
            "state": "SKIPPED",
            "progress": 0
          },
          {
            "id": "sonar",
            "description": null,
            "state": "SKIPPED",
            "progress": 0
          },
          {
            "id": "oss",
            "description": null,
            "state": "SKIPPED",
            "progress": 0
          }
        ]
      }
    },
    "installationProgress":       {
      "createTimestamp": "2021-10-05T19:09:17.323+03:00",
      "id": "2",
      "type": "INSTALL",
      "state": "COMPLETED",
      "stages":          [
        {
          "id": "download_sources",
          "description": "Stage id=download_sources completed successfully",
          "state": "COMPLETED",
          "progress": 100
        },
        {
          "id": "install",
          "description": "Stage id=install completed successfully",
          "state": "COMPLETED",
          "progress": 100
        },
        {
          "id": "checkup",
          "description": "Stage id=checkup completed successfully",
          "state": "COMPLETED",
          "progress": 100
        }
      ]
    },
    "jiraTask": null,
    "owner": "anton",
    "description": "Current machine is free",
    "jiraTaskId": null
  },
  {
    "id": "str3",
    "ip": "192.168.80.3",
    "name": "Linux",
    "type": "DEV",
    "installedArtifact": null,
    "installationProgress": null,
    "jiraTask": null,
    "owner": null,
    "description": "Current machine is free",
    "jiraTaskId": null
  }
]
```