## Получение списка всех команд

```
GET api/team/all
```

OUT
```json
[
{
    "id": "c121312",
    "name": "PRO TEAM",
    "owner": "Ivanov Ivan",
    "owner_contact": "8-800-111-22-222",
    "support": "Sidirov Ivan",
    "support_contact": "8-800-111-22-6666"
}, {
    "id": "c1",
    "name": "ECA",
    "owner": "Victor",
    "owner_contact": "8-800-111-22-333",
    "support": "Anna",
    "support_contact": "8-800-111-22-1477"
}
]
```

## Получение информации о конкретной команде

```
GET api/team/{teamId}
```

OUT

```json
{
    "id": "c1",
    "name": "ECA",
    "owner": "Victor",
    "owner_contact": "8-800-111-22-333",
    "support": "Anna",
    "support_contact": "8-800-111-22-1477"
}
```