# Репозитории

## Получение списка репозиториев команды

```
GET api/repo/all/byTeam?teamId={teamId}
```

OUT:

```json
[
  {
    "id": "repo1",
    "name": "Репозиторий back разработчика",
    "link": "https://bitbucket.org/atrenkunov/repo1"
  },
  {
    "id": "repo2",
    "name": "Репозиторий front разработчика",
    "link": "https://bitbucket.org/atrenkunov/repo2"
  }
]
```

## Получение информации о конкретном репозитории

```
GET api/repo/{repoId}
```

OUT:

```json
{
    "id": "repo1",
    "name": "Репозиторий бека",
    "link": "bitbucket.org/repo1/info",
    "branchList": [
        {
            "repositoryId": "repo1",
            "id": "origin/master",
            "lastCommit": "asdasdsa",
            "moduleVersion": "0.0.1",
            "lastBuild": {
                "createTimestamp": "2021-10-02T08:29:18.5116693Z",
                "id": "0.0.1.2",
                "type": "BUILD",
                "repositoryId": "repo1",
                "branchId": "origin/master",
                "commit": "asdasdsa",
                "moduleVersion": "0.0.1",
                "state": "COMPLETED",
                "stages": [
                    {
                        "id": "unit_tests",
                        "description": null,
                        "state": "SKIPPED",
                        "progress": 0
                    },
                    {
                        "id": "sonar",
                        "description": "Stage id=sonar completed successfully",
                        "state": "COMPLETED",
                        "progress": 100
                    },
                    {
                        "id": "oss",
                        "description": "Stage id=oss completed successfully",
                        "state": "COMPLETED",
                        "progress": 100
                    }
                ]
            }
        },
        {
            "repositoryId": "repo1",
            "id": "origin/develop",
            "lastCommit": "sasacwe",
            "moduleVersion": "0.0.2",
            "lastBuild": {
                "createTimestamp": "2021-10-02T08:29:23.5961127Z",
                "id": "0.0.2.3",
                "type": "BUILD",
                "repositoryId": "repo1",
                "branchId": "origin/develop",
                "commit": "sasacwe",
                "moduleVersion": "0.0.2",
                "state": "COMPLETED",
                "stages": [
                    {
                        "id": "unit_tests",
                        "description": null,
                        "state": "SKIPPED",
                        "progress": 0
                    },
                    {
                        "id": "sonar",
                        "description": "Stage id=sonar completed successfully",
                        "state": "COMPLETED",
                        "progress": 100
                    },
                    {
                        "id": "oss",
                        "description": "Stage id=oss completed successfully",
                        "state": "COMPLETED",
                        "progress": 100
                    }
                ]
            }
        },
        {
            "repositoryId": "repo1",
            "id": "origin/GKR-123",
            "lastCommit": "e2qweds",
            "moduleVersion": "0.0.3",
            "lastBuild": {
                "createTimestamp": "2021-10-02T08:29:31.1505487Z",
                "id": "0.0.3.5",
                "type": "BUILD",
                "repositoryId": "repo1",
                "branchId": "origin/GKR-123",
                "commit": "e2qweds",
                "moduleVersion": "0.0.3",
                "state": "COMPLETED",
                "stages": [
                    {
                        "id": "unit_tests",
                        "description": null,
                        "state": "SKIPPED",
                        "progress": 0
                    },
                    {
                        "id": "sonar",
                        "description": "Stage id=sonar completed successfully",
                        "state": "COMPLETED",
                        "progress": 100
                    },
                    {
                        "id": "oss",
                        "description": "Stage id=oss completed successfully",
                        "state": "COMPLETED",
                        "progress": 100
                    }
                ]
            }
        }
    ]
}
```

## Создание новой ветки в репозитории

```
POST api/repo/{repoId}/branch
```

IN:

```json
{
  "id": "feature/GKR-123",
  "repositoryId": "repo1",
  "sourceId": "develop"
}
```

OUT:

```json
{
  "repositoryId": "repo1",
  "id": "feature/GKR-123",
  "lastCommit": "mlfnwelkfnew;fmew;lfm",
  "moduleVersion": "0.2.0",
  "lastBuild": null
}
```

## Получить информацию о ветке в репозитории

```
POST api/repo/{repoId}/branch/info
```

IN

```json
{
  "repositoryId": "repo1",
  "id": "feature/GKR-123"
}
```

OUT

```json
{
  "repositoryId": "repo1",
  "id": "feature/GKR-123",
  "lastCommit": "mlfnwelkfnew;fmew;lfm",
  "moduleVersion": "0.2.0",
  "lastBuild": null
}
```

## Удалить ветку

```
DELETE api/repo/{repoId}/branch
```

IN

```json
{
  "repositoryId": "repo1",
  "id": "feature/GKR-123"
}
```