# Создать интеграцию

```
POST /api/integration
```

```json
{
    "integrationName": "ЕФС",
    "integrationType": "QUEUE",
    "sourceTeamId": "c121312",
    "destTeamId": "с753648"
}
```

OUT

```json
{
    "id": 3,
    "integrationName": "ЕФС",
    "integrationType": "QUEUE",
    "sourceTeamId": "c121312",
    "destTeamId": "с753648"
}
```

## Удалить интеграцию

DELETE /api/integration/{integrationId}


## Получение информации о интеграции

```
GET api/integration/{integrationId}
```

OUT

```json
{
    "id": 1,
    "integrationName": "ЕФС",
    "integrationType": null,
    "sourceTeamId": "c121312",
    "destTeamId": "с753648",
    "status": null
}
```

## Получение списка всех интеграций команды

```
GET api/integration/all/byTeam={teamId}
```

OUT

```json
[
{
        "id": 1,
        "integrationName": "ЕФС",
        "integrationType": null,
        "sourceTeamId": "c121312",
        "destTeamId": "с753648",
        "status": null
    },
    {
        "id": 2,
        "integrationName": "ЕФС",
        "integrationType": "QUEUE",
        "sourceTeamId": "c121312",
        "destTeamId": "с753648",
        "status": null
    }]
```

## Получение логов интеграции

```
GET api/integration/{integrationId}/log
```

OUT

```json
[
    {
        "id": 1,
        "integrationId": 1,
        "timestamp": "2021-10-02 20:11:44.744",
        "log": "INFO 8784 --- [           main] org.hibernate.dialect.Dialect            : HHH000400: Using dialect: org.hibernate.dialect.SQLiteDialec"
    },
    {
        "id": 2,
        "integrationId": 1,
        "timestamp": "2021-10-02 20:11:44.744",
        "log": "ERROR 8784 --- [           main] org.hibernate.dialect.Dialect            : HHH000400: Using dialect: org.hibernate.dialect.SQLiteDialec"
    },
    {
        "id": 3,
        "integrationId": 1,
        "timestamp": "2021-10-02 20:11:45.744",
        "log": "ERROR 8784 --- [           main] org.hibernate.dialect.Dialect            : HHH000400: Using dialect: org.hibernate.dialect.SQLiteDialec"
    },
    {
        "id": 4,
        "integrationId": 1,
        "timestamp": "2021-10-02 20:11:46.744",
        "log": "ERROR 8784 --- [           main] org.hibernate.dialect.Dialect            : HHH000400: Using dialect: org.hibernate.dialect.SQLiteDialec"
    },
    {
        "id": 5,
        "integrationId": 1,
        "timestamp": "2021-10-02 20:11:47.744",
        "log": "ERROR 8784 --- [           main] org.hibernate.dialect.Dialect            : HHH000400: Using dialect: org.hibernate.dialect.SQLiteDialec"
    }
]
```

## Установить статус интеграции (включить или вывести из строя)

```
POST api/integration/{integrationId}/set/{status}
```

## Получение списка возможных типов интеграций

```
GET api/integration/integrationTypes
```

OUT

```json
[
    "QUEUE",
    "REST",
    "SOAP"
]
```