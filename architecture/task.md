# Работа с задачами

## Получение списка задач

```
GET /api/jira/task/all
```

OUT:

```json
[
  {
    "taskId": "GKR-123",
    "taskName": "Внедрение биометрии",
    "description": "Разработать механизмы биометрии через потоковую камеру",
    "type": "task",
    "version": "0.0.1",
    "lastBuildNumber": "0.0.1.125",
    "taskStatus": "IN PROGRESS",
    "jiraUrl": "http://jira.ru/GKR-123",
    "selected": "true",
    "branch": {
      "repositoryId": "repo1",
      "id": "origin/feature/GKR-123",
      "lastCommit": "e2qweds",
      "moduleVersion": "0.0.3",
      "lastBuild": {
        "createTimestamp": "2021-10-05T18:03:16.281Z",
        "id": "0.0.3.2",
        "type": "BUILD",
        "repositoryId": "repo1",
        "branchId": "origin/GKR-123",
        "commit": "e2qweds",
        "moduleVersion": "0.0.3",
        "state": "COMPLETED",
        "stages": [
          {
            "id": "unit_tests",
            "description": null,
            "state": "SKIPPED",
            "progress": 0
          },
          {
            "id": "sonar",
            "description": null,
            "state": "SKIPPED",
            "progress": 0
          },
          {
            "id": "oss",
            "description": null,
            "state": "SKIPPED",
            "progress": 0
          }
        ]
      }
    }
  },
  {
    "taskId": "GKR-321",
    "taskName": "Разработка  аутентификации",
    "description": "Разработать аутентификацию в приложении",
    "type": "story",
    "branch": null,
    "version": null,
    "lastBuildNumber": null,
    "taskStatus": "OPENED",
    "jiraUrl": "http://jira.ru/GKR-123",
    "selected": "false",
    "branch": null
  }
]
```

## Получение списка задач по команде

```
GET /api/jira/task/byTeamId?teamId={teamId}
```

OUT:

```json
[
  {
    "taskId": "GKR-123",
    "taskName": "Внедрение биометрии",
    "description": "Разработать механизмы биометрии через потоковую камеру",
    "type": "task",
    "version": "0.0.1",
    "lastBuildNumber": "0.0.1.125",
    "taskStatus": "IN PROGRESS",
    "jiraUrl": "http://jira.ru/GKR-123",
    "selected": "false",
    "branch": {
      "repositoryId": "repo1",
      "id": "origin/feature/GKR-123",
      "lastCommit": "e2qweds",
      "moduleVersion": "0.0.3",
      "lastBuild": {
        "createTimestamp": "2021-10-05T18:03:16.281Z",
        "id": "0.0.3.2",
        "type": "BUILD",
        "repositoryId": "repo1",
        "branchId": "origin/GKR-123",
        "commit": "e2qweds",
        "moduleVersion": "0.0.3",
        "state": "COMPLETED",
        "stages": [
          {
            "id": "unit_tests",
            "description": null,
            "state": "SKIPPED",
            "progress": 0
          },
          {
            "id": "sonar",
            "description": null,
            "state": "SKIPPED",
            "progress": 0
          },
          {
            "id": "oss",
            "description": null,
            "state": "SKIPPED",
            "progress": 0
          }
        ]
      }
    }
  },
  {
    "taskId": "GKR-321",
    "taskName": "Разработка  аутентификации",
    "description": "Разработать аутентификацию в приложении",
    "type": "story",
    "branch": null,
    "version": null,
    "lastBuildNumber": null,
    "taskStatus": "OPENED",
    "jiraUrl": "http://jira.ru/GKR-123",
    "selected": "false"
  }
]
```

## Получение списка взятых в работу задач по команде

```
GET /api/jira/task/byTeamIdInWork?teamId={teamId}&selected=true
```

OUT:

```json
[
  {
    "taskId": "GKR-123",
    "taskName": "Внедрение биометрии",
    "description": "Разработать механизмы биометрии через потоковую камеру",
    "type": "task",
    "branch": "feature/GKR-123",
    "version": "0.0.1",
    "lastBuildNumber": "0.0.1.125",
    "taskStatus": "IN PROGRESS",
    "jiraUrl": "http://jira.ru/GKR-123",
    "selected": "false",
    "branch": null
  },
  {
    "taskId": "GKR-321",
    "taskName": "Разработка  аутентификации",
    "description": "Разработать аутентификацию в приложении",
    "type": "story",
    "branch": null,
    "version": null,
    "lastBuildNumber": null,
    "taskStatus": "OPENED",
    "jiraUrl": "http://jira.ru/GKR-123",
    "selected": "false",
    "branch": null
  }
]
```

## Создание новой задачи

```
POST api/jira/task/
```

IN:

```json
{
  "name": "Разработка  аутентификации",
  "description": "Разработать аутентификацию в приложении",
  "type": "story"
}
```

OUT:

```json
{
  "taskId": "GKR-321",
  "name": "Разработка  аутентификации",
  "description": "Разработать аутентификацию в приложении",
  "type": "story",
  "branch": null,
  "version": null,
  "lastBuildNumber": null,
  "taskStatus": "OPENED",
  "jiraUrl": "http://jira.ru/GKR-123",
  "selected": "false"
}
```

## Выбор задачи (взятие в работу, экран планирования)

```
POST api/jira/task/select
```

IN:

```json
{
  "taskId": "GKR-321",
  "selected": true or false
}
```

OUT:

```json
{
  "taskId": "GKR-321",
  "name": "Разработка  аутентификации",
  "description": "Разработать аутентификацию в приложении",
  "type": "story",
  "branch": null,
  "version": null,
  "lastBuildNumber": null,
  "taskStatus": "OPENED",
  "jiraUrl": "http://jira.ru/GKR-123",
  "selected": "true"
}
```