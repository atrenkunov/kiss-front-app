# Получение списка артефактов команды

```
GET /api/nexus/artifact/all/byTeam?teamId={teamId}
```

OUT:

```json
{
    "": {
        "repo1_origin/develop": [{
                "id": "fa9fe8f8-b1b1-420b-99ce-cc1588d5ca19",
                "name": "0.0.2.3",
                "link": "http://nexus.ru/builds/0.0.2.3/summary",
                "timestamp": "2021-10-02T11:30:06.6009263+03:00",
                "buildInfo": {
                    "createTimestamp": "2021-10-02T08:29:23.5961127Z",
                    "id": "3",
                    "type": "BUILD",
                    "repositoryId": "repo1",
                    "branchId": "origin/develop",
                    "commit": "sasacwe",
                    "moduleVersion": "0.0.2",
                    "state": "COMPLETED",
                    "stages": [{
                            "id": "unit_tests",
                            "description": null,
                            "state": "SKIPPED",
                            "progress": 0
                        }, {
                            "id": "sonar",
                            "description": "Stage id=sonar completed successfully",
                            "state": "COMPLETED",
                            "progress": 100
                        }, {
                            "id": "oss",
                            "description": "Stage id=oss completed successfully",
                            "state": "COMPLETED",
                            "progress": 100
                        }
                    ]
                }
            }
        ],
        "repo1_origin/master": [{
                "id": "96f4113a-a8a9-46a4-90ca-5afd3a041f4f",
                "name": "0.0.1.2",
                "link": "http://nexus.ru/builds/0.0.1.2/summary",
                "timestamp": "2021-10-02T11:29:47.119891+03:00",
                "buildInfo": {
                    "createTimestamp": "2021-10-02T08:29:18.5116693Z",
                    "id": "2",
                    "type": "BUILD",
                    "repositoryId": "repo1",
                    "branchId": "origin/master",
                    "commit": "asdasdsa",
                    "moduleVersion": "0.0.1",
                    "state": "COMPLETED",
                    "stages": [{
                            "id": "unit_tests",
                            "description": null,
                            "state": "SKIPPED",
                            "progress": 0
                        }, {
                            "id": "sonar",
                            "description": "Stage id=sonar completed successfully",
                            "state": "COMPLETED",
                            "progress": 100
                        }, {
                            "id": "oss",
                            "description": "Stage id=oss completed successfully",
                            "state": "COMPLETED",
                            "progress": 100
                        }
                    ]
                }
            }
        ]
    },
    "GKR-123": {
        "repo1_origin/GKR-123": [{
                "id": "d8242712-3776-46eb-b373-238d1333a45a",
                "name": "0.0.3.4",
                "link": "http://nexus.ru/builds/0.0.3.4/summary",
                "timestamp": "2021-10-02T11:30:15.763973+03:00",
                "buildInfo": {
                    "createTimestamp": "2021-10-02T08:29:30.4131148Z",
                    "id": "4",
                    "type": "BUILD",
                    "repositoryId": "repo1",
                    "branchId": "origin/GKR-123",
                    "commit": "e2qweds",
                    "moduleVersion": "0.0.3",
                    "state": "COMPLETED",
                    "stages": [{
                            "id": "unit_tests",
                            "description": null,
                            "state": "SKIPPED",
                            "progress": 0
                        }, {
                            "id": "sonar",
                            "description": "Stage id=sonar completed successfully",
                            "state": "COMPLETED",
                            "progress": 100
                        }, {
                            "id": "oss",
                            "description": "Stage id=oss completed successfully",
                            "state": "COMPLETED",
                            "progress": 100
                        }
                    ]
                }
            }, {
                "id": "d28349d7-32ff-45a9-8930-824f473b691a",
                "name": "0.0.3.5",
                "link": "http://nexus.ru/builds/0.0.3.5/summary",
                "timestamp": "2021-10-02T11:30:35.2804353+03:00",
                "buildInfo": {
                    "createTimestamp": "2021-10-02T08:29:31.1505487Z",
                    "id": "5",
                    "type": "BUILD",
                    "repositoryId": "repo1",
                    "branchId": "origin/GKR-123",
                    "commit": "e2qweds",
                    "moduleVersion": "0.0.3",
                    "state": "COMPLETED",
                    "stages": [{
                            "id": "unit_tests",
                            "description": null,
                            "state": "SKIPPED",
                            "progress": 0
                        }, {
                            "id": "sonar",
                            "description": "Stage id=sonar completed successfully",
                            "state": "COMPLETED",
                            "progress": 100
                        }, {
                            "id": "oss",
                            "description": "Stage id=oss completed successfully",
                            "state": "COMPLETED",
                            "progress": 100
                        }
                    ]
                }
            }
        ]
    }
}
```