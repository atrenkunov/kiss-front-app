# Aвторизация

## Информация о пользователе

```GET /api/auth/info```

OUT:
```json
{
    "userId": "u332141",
    "firstName": "Anton",
    "lastName": "Ivanov",
    "team": {
        "id": "c121312",
        "name": "СБОЛ ПРО",
        "owner": "Petrov Ivan",
        "phone": "8-123-321-11-00"
    }
}
```