package ru.sber.kiss.converter;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import ru.sber.kiss.dto.BranchInfo;
import ru.sber.kiss.repo.dto.BranchDTO;

@Component
public class BranchDTOToBranchInfoConverter implements Converter<BranchDTO, BranchInfo> {

    @Override
    public BranchInfo convert(BranchDTO source) {
        return BranchInfo.builder()
                .repositoryId(source.getRepositoryId())
                .id(source.getId())
                .moduleVersion(source.getModuleVersion())
                .lastCommit(source.getLastCommit())
                .build();
    }
}
