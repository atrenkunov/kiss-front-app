package ru.sber.kiss.converter;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import ru.sber.kiss.dto.RepositoryShortInfo;
import ru.sber.kiss.repo.dto.RepositoryShortDTO;

@Component
public class RepositoryShortDTOToRepositoryShortInfoConverter implements Converter<RepositoryShortDTO, RepositoryShortInfo> {

    @Override
    public RepositoryShortInfo convert(RepositoryShortDTO dto) {
        return new RepositoryShortInfo(dto.getId(), dto.getName(), dto.getLink());
    }
}
