package ru.sber.kiss.converter;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import ru.sber.kiss.auth.dto.AuthorizationDTO;
import ru.sber.kiss.team.dto.TeamDTO;
import ru.sber.kiss.dto.AuthorizationInfo;
import ru.sber.kiss.dto.TeamInfo;

@Component
public class AuthorizationDTOtoAuthorizationInfoConverter implements Converter<AuthorizationDTO, AuthorizationInfo> {

    @Override
    public AuthorizationInfo convert(AuthorizationDTO dto) {
        return new AuthorizationInfo(dto.getUserId(), dto.getFirstName(), dto.getLastName(), getCommandInfo(dto.getTeam()));
    }

    private TeamInfo getCommandInfo(TeamDTO dto) {
        if (dto == null) {
            return null;
        }

        return TeamInfo.builder()
                .id(dto.getId())
                .name(dto.getName())
                .owner(dto.getOwner())
                .phone(dto.getOwnerContact())
                .build();
    }
}
