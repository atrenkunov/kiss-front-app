package ru.sber.kiss.dto;

import lombok.Builder;
import ru.sber.kiss.executor.core.Task;

@Builder
public class BranchInfo {

    private String repositoryId;
    private String id;
    private String lastCommit;
    private String moduleVersion;
    private Task lastBuild;

    public String getRepositoryId() {
        return repositoryId;
    }

    public void setRepositoryId(String repositoryId) {
        this.repositoryId = repositoryId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLastCommit() {
        return lastCommit;
    }

    public void setLastCommit(String lastCommit) {
        this.lastCommit = lastCommit;
    }

    public String getModuleVersion() {
        return moduleVersion;
    }

    public void setModuleVersion(String moduleVersion) {
        this.moduleVersion = moduleVersion;
    }

    public Task getLastBuild() {
        return lastBuild;
    }

    public void setLastBuild(Task lastBuild) {
        this.lastBuild = lastBuild;
    }
}
