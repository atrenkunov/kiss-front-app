package ru.sber.kiss.dto;

import lombok.Builder;

import java.util.ArrayList;
import java.util.List;

@Builder
public class RepositoryInfo {

    private String id;
    private String name;
    private String link;
    private List<BranchInfo> branchList;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public List<BranchInfo> getBranchList() {
        if (branchList == null) {
            branchList = new ArrayList<>();
        }
        return branchList;
    }

    public void setBranchList(List<BranchInfo> branchList) {
        this.branchList = branchList;
    }
}
