package ru.sber.kiss.dto;

/**
 * Authorization info, e.g. login, role, team ...
 */
public class AuthorizationInfo {

    private String userId;
    private String firstName;
    private String lastName;
    private TeamInfo team;

    public AuthorizationInfo(String userId, String firstName, String lastName, TeamInfo team) {
        this.userId = userId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.team = team;
    }

    public String getUserId() {
        return userId;
    }

    public TeamInfo getTeam() {
        return team;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }


    @Override
    public String toString() {
        return "AuthorizationInfo{" +
                "userId='" + userId + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", team=" + team +
                '}';
    }
}
