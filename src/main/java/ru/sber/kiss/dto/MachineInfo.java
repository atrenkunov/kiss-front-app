package ru.sber.kiss.dto;

import lombok.Builder;
import ru.sber.kiss.executor.core.Task;
import ru.sber.kiss.nexus.dto.ArtifactInfo;
import ru.sber.kiss.task.dto.JiraTaskDTO;

@Builder
public class MachineInfo {

    private String id;
    private String ip;
    private String name;
    private String type;
    private ArtifactInfo installedArtifact;
    private Task installationProgress;
    private JiraTaskDTO jiraTask;
    private String owner;
    private String description;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArtifactInfo getInstalledArtifact() {
        return installedArtifact;
    }

    public void setInstalledArtifact(ArtifactInfo installedArtifact) {
        this.installedArtifact = installedArtifact;
    }

    public JiraTaskDTO getJiraTaskId() {
        return jiraTask;
    }

    public void setJiraTaskId(JiraTaskDTO jiraTaskId) {
        this.jiraTask = jiraTaskId;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getDescription() {
        if (description == null) {
            return "Current machine is free";
        }
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Task getInstallationProgress() {
        return installationProgress;
    }

    public JiraTaskDTO getJiraTask() {
        return jiraTask;
    }

    public void setInstallationProgress(Task installationProgress) {
        this.installationProgress = installationProgress;
    }

    public void setJiraTask(JiraTaskDTO jiraTask) {
        this.jiraTask = jiraTask;
    }
}
