package ru.sber.kiss.dto;

import ru.sber.kiss.nexus.dto.ArtifactInfo;

import java.util.ArrayList;
import java.util.List;

public class BranchArtifacts {

    private String repositoryId;
    private String branchId;
    private List<ArtifactInfo> artifacts;

    public BranchArtifacts(String repositoryId, String branchId, List<ArtifactInfo> artifacts) {
        this.repositoryId = repositoryId;
        this.branchId = branchId;
        this.artifacts = artifacts;
    }

    public String getRepositoryId() {
        return repositoryId;
    }

    public String getBranchId() {
        return branchId;
    }

    public List<ArtifactInfo> getArtifacts() {
        if (artifacts == null) {
            artifacts = new ArrayList<>();
        }
        return artifacts;
    }
}
