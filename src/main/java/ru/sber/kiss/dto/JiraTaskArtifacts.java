package ru.sber.kiss.dto;

import java.util.ArrayList;
import java.util.List;

public class JiraTaskArtifacts {

    private String id;
    private String name;
    private List<BranchArtifacts> branches;

    public JiraTaskArtifacts(String id, String name, List<BranchArtifacts> branches) {
        this.id = id;
        this.name = name;
        this.branches = branches;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public List<BranchArtifacts> getBranches() {
        if (branches == null) {
            branches = new ArrayList<>();
        }
        return branches;
    }
}
