package ru.sber.kiss.dto;

import lombok.Builder;

@Builder
public class JiraTaskInfo {

    private String taskId;
    private String teamId;
    private String taskName;
    private String description;
    private String type;
    private boolean selected;
    private String taskStatus;
    private BranchInfo branch;
    private String jiraUrl;

    public String getTaskId() {
        return taskId;
    }

    public String getTeamId() {
        return teamId;
    }

    public String getTaskName() {
        return taskName;
    }

    public String getDescription() {
        return description;
    }

    public String getType() {
        return type;
    }

    public BranchInfo getBranch() {
        return branch;
    }

    public boolean isSelected() {
        return selected;
    }

    public String getTaskStatus() {
        return taskStatus;
    }

    public String getJiraUrl() {
        return jiraUrl;
    }
}
