package ru.sber.kiss.integration.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.sber.kiss.integration.dao.entity.IntegrationEntity;

import java.util.List;

public interface IntegrationRepository extends JpaRepository<IntegrationEntity, Integer> {
    List<IntegrationEntity> findBySourceTeamId(String sourceTeamId);
}
