package ru.sber.kiss.integration.dao.entity;

public enum IntegrationType {
    QUEUE, REST, SOAP
}
