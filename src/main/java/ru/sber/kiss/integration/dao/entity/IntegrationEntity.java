package ru.sber.kiss.integration.dao.entity;

import javax.persistence.*;

@Entity
@Table(name = "integration")
public class IntegrationEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "integration_name")
    private String integrationName;

    @Column(name = "integration_type")
    @Enumerated(EnumType.STRING)
    private IntegrationType integrationType;

    @Column(name = "source_team_id")
    private String sourceTeamId;

    @Column(name = "destination_team_id")
    private String destTeamId;

    @Column(name = "status")
    private String status;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSourceTeamId() {
        return sourceTeamId;
    }

    public void setSourceTeamId(String sourceTeamId) {
        this.sourceTeamId = sourceTeamId;
    }

    public String getDestTeamId() {
        return destTeamId;
    }

    public void setDestTeamId(String destTeamId) {
        this.destTeamId = destTeamId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getIntegrationName() {
        return integrationName;
    }

    public void setIntegrationName(String integrationName) {
        this.integrationName = integrationName;
    }

    public IntegrationType getIntegrationType() {
        return integrationType;
    }

    public void setIntegrationType(IntegrationType integrationType) {
        this.integrationType = integrationType;
    }
}
