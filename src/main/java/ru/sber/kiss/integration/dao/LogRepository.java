package ru.sber.kiss.integration.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.sber.kiss.integration.dao.entity.LogEntity;

import java.util.List;

public interface LogRepository extends JpaRepository<LogEntity, Integer> {
    List<LogEntity> findByIntegrationId(String integrationId);
}
