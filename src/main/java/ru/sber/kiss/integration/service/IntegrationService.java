package ru.sber.kiss.integration.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.sber.kiss.integration.dao.IntegrationRepository;
import ru.sber.kiss.integration.dao.LogRepository;
import ru.sber.kiss.integration.dao.entity.IntegrationEntity;
import ru.sber.kiss.integration.dao.entity.IntegrationType;
import ru.sber.kiss.integration.dao.entity.LogEntity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class IntegrationService {

    @Autowired
    private IntegrationRepository integrationRepository;

    @Autowired
    private LogRepository logRepository;

    public IntegrationEntity createIntegration(IntegrationEntity integration) {
        integration = integrationRepository.save(integration);
        createFakeLog(integration.getId());
        return integration;
    }

    public IntegrationEntity getIntegration(Integer integrationId) {
        IntegrationEntity integration = integrationRepository.findById(integrationId).get();
        return integration;
    }

    public List<IntegrationEntity> getAllIntegrationsByTeamId(String teamId) {
        return integrationRepository.findBySourceTeamId(teamId);
    }

    public List<LogEntity> getIntegrationLog(String integrationId) {
        return logRepository.findByIntegrationId(integrationId);
    }

    public void deleteIntegration(Integer integrationId) {
        integrationRepository.deleteById(integrationId);
    }

    public String setIntegrationStatus(Integer integrationId, String state) {
        IntegrationEntity integration = integrationRepository.findById(integrationId).get();
        integration.setStatus(state);
        return integrationRepository.save(integration).getStatus();
    }

    public List<String> getIntegrationTypes() {
        return Arrays.asList(IntegrationType.values())
                .stream()
                .map(Enum::name)
                .collect(Collectors.toList());
    }

    private void createFakeLog(Integer integrationId) {
        List<LogEntity> fakeLogs = new ArrayList<>();
        for (int i = 0; i < 100; i++) {
            LogEntity log = new LogEntity();
            log.setIntegrationId(integrationId.toString());
            log.setTimestamp("2021-10-02 20:11:44.744");
            log.setLog("ERROR 8784 --- [           main] org.hibernate.dialect.Dialect            : HHH000400: Using dialect: org.hibernate.dialect.SQLiteDialec");
            fakeLogs.add(log);
        }
        logRepository.saveAll(fakeLogs);
    }
}
