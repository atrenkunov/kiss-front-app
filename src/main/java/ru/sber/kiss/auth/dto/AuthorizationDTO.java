package ru.sber.kiss.auth.dto;

import ru.sber.kiss.team.dto.TeamDTO;

/**
 * Authorization DTO, e.g. login, role, team ...
 */
public class AuthorizationDTO {

    private String userId;
    private String firstName;
    private String lastName;
    private TeamDTO team;

    public AuthorizationDTO(String userId, String firstName, String lastName, TeamDTO team) {
        this.userId = userId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.team = team;
    }

    public String getUserId() {
        return userId;
    }

    public TeamDTO getTeam() {
        return team;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }


    @Override
    public String toString() {
        return "AuthorizationDTO{" +
                "userId='" + userId + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", team=" + team +
                '}';
    }
}
