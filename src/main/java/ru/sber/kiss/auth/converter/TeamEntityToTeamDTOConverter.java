package ru.sber.kiss.auth.converter;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import ru.sber.kiss.team.dto.TeamDTO;
import ru.sber.kiss.team.dao.entity.TeamEntity;

@Component
public class TeamEntityToTeamDTOConverter implements Converter<TeamEntity, TeamDTO> {

    @Override
    public TeamDTO convert(TeamEntity entity) {

        return TeamDTO.builder()
                .id(entity.getId())
                .name(entity.getName())
                .owner(entity.getOwner())
                .ownerContact(entity.getOwnerContact())
                .support(entity.getSupport())
                .supportContact(entity.getSupportContact())
                .build();
    }
}
