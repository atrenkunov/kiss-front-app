package ru.sber.kiss.auth.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Component;
import ru.sber.kiss.auth.dto.AuthorizationDTO;
import ru.sber.kiss.team.dto.TeamDTO;
import ru.sber.kiss.team.service.TeamService;

@Component
public class AuthService {

    private static final String COMMAND_ID = "c121312";

    @Autowired
    private ConversionService conversionService;

    @Autowired
    private TeamService teamService;

    public AuthorizationDTO getUserInfo() {

        TeamDTO team = teamService.findTeam(COMMAND_ID);
        return new AuthorizationDTO("u332141", "Anton", "Petrov", team);
    }
}
