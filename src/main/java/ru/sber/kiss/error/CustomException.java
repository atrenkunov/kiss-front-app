package ru.sber.kiss.error;

public class CustomException extends RuntimeException {

    private final String code;

    public CustomException(String msg, String code) {
        super(msg);
        this.code = code;
    }

    public CustomException(String msg, Throwable ex, String code) {
        super(msg, ex);
        this.code = code;
    }

    public String getCode() {
        return code;
    }
}
