package ru.sber.kiss.error;

public class Errors {

    public static final String BRANCH_ALREADY_EXISTS = "BRANCH_ALREADY_EXISTS";
    public static final String REPOSITORY_NOT_FOUND = "REPOSITORY_NOT_FOUND";
    public static final String BRANCH_NOT_FOUND = "BRANCH_NOT_FOUND";
    public static final String JIRA_TASK_NOT_FOUND = "JIRA_TASK_NOT_FOUND";
    public static final String JIRA_TASK_ALREADY_EXISTS = "JIRA_TASK_ALREADY_EXISTS";
    public static final String MACHINE_ALREADY_EXISTS = "MACHINE_ALREADY_EXISTS";
    public static final String MACHINE_NOT_FOUND = "MACHINE_NOT_FOUND";
}
