package ru.sber.kiss.executor;

public class StageInfo {

    private String id;
    private String name;

    public StageInfo(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
