package ru.sber.kiss.executor.core;

public enum StageState {
    WAITING,
    RUNNING,
    SKIPPED,
    COMPLETED,
    FAILED
}
