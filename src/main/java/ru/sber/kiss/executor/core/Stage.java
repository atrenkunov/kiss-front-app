package ru.sber.kiss.executor.core;

public class Stage {

    private String id;
    private volatile String description;
    private volatile StageState state;
    private volatile int progress;

    public Stage() {
    }

    public Stage(String id, StageState state) {
        this.id = id;
        this.state = state;
        this.progress = 0;
    }

    public Stage(String id) {
        this(id, StageState.WAITING);
    }

    public synchronized void increaseProgress(int value) {

        if (state.equals(StageState.WAITING)) {
            state = StageState.RUNNING;
        }

        progress += value;
        if (progress >= 100) {
            progress = 100;
            state = StageState.COMPLETED;
            description = "Stage id=" + id + " completed successfully";
        }
    }

    public void fail(String msg) {
        state = StageState.FAILED;
        description = "Stage id=" + id + " failed reason=" + msg;
    }

    public String getId() {
        return id;
    }

    public String getDescription() {
        return description;
    }

    public StageState getState() {
        return state;
    }

    public synchronized int getProgress() {
        return progress;
    }
}
