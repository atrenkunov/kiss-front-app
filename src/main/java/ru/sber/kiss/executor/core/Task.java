package ru.sber.kiss.executor.core;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.extern.slf4j.Slf4j;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.function.Consumer;


@Slf4j
public class Task {

    public enum Type {
        BUILD,
        INSTALL
    }

    private final OffsetDateTime createTimestamp = OffsetDateTime.now();
    private String id;
    private Type type;

    @JsonIgnore
    private String teamId;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String repositoryId;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String branchId;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String commit;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String moduleVersion;

    private volatile StageState state;

    private List<Stage> stages;

    @JsonIgnore
    private Consumer<Task> onComplete;

    public Task() {
    }

    public Task(String id, Type type, String teamId, String repositoryId, String branchId, String commit, String moduleVersion, List<Stage> stages) {
        this.id = id;
        this.teamId = teamId;
        this.type = type;
        this.repositoryId = repositoryId;
        this.branchId = branchId;
        this.commit = commit;
        this.moduleVersion = moduleVersion;
        this.stages = Objects.nonNull(stages) ? stages : new ArrayList<>();
        this.state = StageState.WAITING;
    }

    public Task(String id, Type type, List<Stage> stages) {
        this(id, type, null, null, null, null, null, stages);
    }

    public void execute() {

        try {
            log.info("Task id={} stages={} started", id, stages.size());

            Thread.sleep(500);
            state = StageState.RUNNING;

            for (int i = 0; i < stages.size(); ++i) {
                Stage stage = stages.get(i);
                if (stage.getState().equals(StageState.SKIPPED)) {
                    continue;
                }

                Thread.sleep(500);

                while (!StageState.FAILED.equals(stage.getState()) && !StageState.COMPLETED.equals(stage.getState())) {
                    Thread.sleep(500);
                    stage.increaseProgress(10);
                }
                log.info("Task id={} stage={} completed", id, stage.getId());
            }
            state = StageState.COMPLETED;
            if (onComplete != null) {
                onComplete.accept(this);
            }
            log.info("Task id={} completed", id);
        } catch (Exception ex) {
            log.error("Failed to .execute id={} stages.size()={} error={}", id, stages.size(), ex.toString(), ex);
        }
    }

    public String getModuleVersion() {
        return moduleVersion;
    }

    public String getId() {
        return id;
    }

    public StageState getState() {
        return state;
    }

    public List<Stage> getStages() {
        if (stages == null) {
            stages = new ArrayList<>();
        }
        return stages;
    }

    public OffsetDateTime getCreateTimestamp() {
        return createTimestamp;
    }

    public String getTeamId() {
        return teamId;
    }

    public String getCommit() {
        return commit;
    }

    public Type getType() {
        return type;
    }

    public void setOnComplete(Consumer<Task> onComplete) {
        this.onComplete = onComplete;
    }

    public String getRepositoryId() {
        return repositoryId;
    }

    public String getBranchId() {
        return branchId;
    }

    public void setStages(List<Stage> stages) {
        this.stages = stages;
    }

    public void setId(String id) {
        this.id = id;
    }
}
