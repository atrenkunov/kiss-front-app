package ru.sber.kiss.executor.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.sber.kiss.executor.StageInfo;
import ru.sber.kiss.executor.core.Stage;
import ru.sber.kiss.executor.core.StageState;
import ru.sber.kiss.executor.core.Task;
import ru.sber.kiss.executor.dto.CreateBuildRq;
import ru.sber.kiss.nexus.dto.ArtifactInfo;
import ru.sber.kiss.nexus.service.NexusService;
import ru.sber.kiss.repo.dto.BranchDTO;
import ru.sber.kiss.repo.service.RepositoryService;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

@Slf4j
@Component
public class ExecutorService {

    private final java.util.concurrent.ExecutorService scheduler = Executors.newFixedThreadPool(2);

    private final Map<String, Task> taskCache = new HashMap<>();
    private final Map<String, String> taskArtifacts = Collections.synchronizedMap(new HashMap<>());

    @Autowired
    private RepositoryService repositoryService;

    @Autowired
    private NexusService nexusService;

    @Autowired
    private ObjectMapper mapper;

    public List<StageInfo> getBuildStages() {

        return Arrays.asList(
                new StageInfo("download_sources", "Скачиваю ветку из репозитория"),
                new StageInfo("compile", "Компиляция программы"),
                new StageInfo("unit_tests", "Запуск unit тестов"),
                new StageInfo("sonar", "Запуск сонара"),
                new StageInfo("oss", "Запуск OSS"),
                new StageInfo("publish", "Публикация артефактов")
        );
    }

    public List<StageInfo> getInstallStages() {

        return Arrays.asList(
                new StageInfo("download_sources", "Скачиваю артефакт"),
                new StageInfo("install", "Установка"),
                new StageInfo("checkup", "Проверка установки")
        );
    }

    public List<String> getQualityGates() {
        return Arrays.asList(
                "unit_tests",
                "sonar",
                "oss"
        );
    }

    public Task buildBranch(CreateBuildRq request) {

        List<String> skipStages = getQualityGates().stream()
                .filter(s -> !request.getCheckList().contains(s))
                .collect(Collectors.toList());

        List<Stage> stages = getBuildStages().stream()
                .map(StageInfo::getId)
                .map(stageId -> {
                    if (skipStages.contains(stageId)) {
                        return new Stage(stageId, StageState.SKIPPED);
                    } else {
                        return new Stage(stageId);
                    }
                })
                .collect(Collectors.toList());

        Task task;

        String moduleVersion = "unknown";
        String commit = "unknown";
        BranchDTO branchInfo = repositoryService.getBranch(request.getRepositoryId(), request.getBranchId());
        if (Objects.nonNull(branchInfo)) {
            moduleVersion = branchInfo.getModuleVersion();
            commit = branchInfo.getLastCommit();
        }

        synchronized (taskCache) {
            String teamId = request.getTeamId();
            task = new Task(String.valueOf(taskCache.size() + 1), Task.Type.BUILD, teamId, request.getRepositoryId(), request.getBranchId(), commit, moduleVersion, stages);
            task.setOnComplete(t -> {
                Task lastBuild = makeDeepTaskCopy(t);
                lastBuild.getStages().removeIf(stage -> !getQualityGates().contains(stage.getId()));

                ArtifactInfo artifact = nexusService.put(teamId, new ArtifactInfo(t.getModuleVersion() + "." + t.getId(), lastBuild));
                taskArtifacts.put(t.getId(), artifact.getId());
            });

            taskCache.put(task.getId(), task);
            scheduler.submit(task::execute);
        }

        return task;
    }

    public Task installArtifact() {

        Task task;
        List<Stage> stages = getInstallStages().stream()
                .map(StageInfo::getId)
                .map(Stage::new)
                .collect(Collectors.toList());

        synchronized (taskCache) {
            task = new Task(String.valueOf(taskCache.size() + 1), Task.Type.INSTALL, stages);
            taskCache.put(task.getId(), task);
            scheduler.submit(task::execute);
        }

        return task;
    }

    public Task getTaskStatus(String buildId) {
        synchronized (taskCache) {
            return taskCache.get(buildId);
        }
    }

    public ArtifactInfo getTaskResult(String buildId) {

        String artifactId = taskArtifacts.get(buildId);
        return nexusService.get(artifactId);
    }

    public List<Task> getBuildsByTeam(String teamId) {

        synchronized (taskCache) {
            return taskCache.values().stream()
                    .filter(t -> Objects.equals(t.getTeamId(), teamId))
                    .filter(t -> Task.Type.BUILD.equals(t.getType()))
                    .collect(Collectors.toList());
        }
    }

    public Task getLastBranchBuild(String repositoryId, String branchId) {

        synchronized (taskCache) {
            return taskCache.values().stream()
                    .filter(t -> Objects.equals(StageState.COMPLETED, t.getState()))
                    .filter(t -> Objects.equals(repositoryId, t.getRepositoryId()))
                    .filter(t -> Objects.equals(branchId, t.getBranchId()))
                    .max(Comparator.comparing(Task::getCreateTimestamp))
                    .orElse(null);
        }
    }

    private Task makeDeepTaskCopy(Task task) {
        try {
            return mapper.readValue(mapper.writeValueAsBytes(task), Task.class);
        } catch (IOException ignored) {
            return null;
        }
    }
}
