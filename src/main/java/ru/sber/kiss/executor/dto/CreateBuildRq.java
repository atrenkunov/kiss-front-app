package ru.sber.kiss.executor.dto;

import java.util.ArrayList;
import java.util.List;

public class CreateBuildRq {

    private String teamId;
    private String repositoryId;
    private String branchId;
    private List<String> checkList;

    public String getTeamId() {
        return teamId;
    }

    public String getRepositoryId() {
        return repositoryId;
    }

    public String getBranchId() {
        return branchId;
    }

    public List<String> getCheckList() {
        if (checkList == null) {
            checkList = new ArrayList<>();
        }
        return checkList;
    }
}
