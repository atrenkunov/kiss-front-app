package ru.sber.kiss.task.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import ru.sber.kiss.task.dao.entity.JiraTaskEntity;

import java.util.List;

public interface JiraTaskRepository extends JpaRepository<JiraTaskEntity, String> {

    List<JiraTaskEntity> findByTeamId(String teamId);
    List<JiraTaskEntity> findByTeamIdAndSelectedEquals(String teamId, Boolean selected);

    @Query(value = "SELECT max(taskId) FROM JiraTaskEntity")
    public String maxId();
}
