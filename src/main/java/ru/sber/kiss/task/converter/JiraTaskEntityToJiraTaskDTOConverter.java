package ru.sber.kiss.task.converter;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import ru.sber.kiss.task.dto.JiraTaskDTO;
import ru.sber.kiss.task.dao.entity.JiraTaskEntity;

@Component
public class JiraTaskEntityToJiraTaskDTOConverter implements Converter<JiraTaskEntity, JiraTaskDTO> {

    @Override
    public JiraTaskDTO convert(JiraTaskEntity entity) {
        return JiraTaskDTO.builder()
                .taskId(entity.getTaskId())
                .teamId(entity.getTeamId())
                .selected(entity.getSelected())
                .taskName(entity.getTaskName())
                .description(entity.getDescription())
                .type(entity.getType())
                .taskStatus(entity.getTaskStatus())
                .build();
    }
}
