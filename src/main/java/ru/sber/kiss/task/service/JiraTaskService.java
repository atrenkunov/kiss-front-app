package ru.sber.kiss.task.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import ru.sber.kiss.error.CustomException;
import ru.sber.kiss.error.Errors;
import ru.sber.kiss.task.dao.JiraTaskRepository;
import ru.sber.kiss.task.dto.JiraTaskDTO;
import ru.sber.kiss.task.dao.entity.JiraTaskEntity;
import ru.sber.kiss.task.dto.SelectJiraRq;

import javax.persistence.Column;
import javax.persistence.Id;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class JiraTaskService {

    @Autowired
    private ConversionService conversionService;

    @Autowired
    private JiraTaskRepository jiraTaskRepository;

    public List<JiraTaskDTO> getAllTask() {
        return jiraTaskRepository.findAll().stream()
                .map(entity -> conversionService.convert(entity, JiraTaskDTO.class))
                .collect(Collectors.toList());
    }

    public JiraTaskDTO selectJiraTask(SelectJiraRq request) {
        JiraTaskEntity entity = jiraTaskRepository.findById(request.getTaskId()).orElse(null);
        if (entity == null) {
            throw new CustomException("JIra task taskId=" + request.getTaskId() + " not found", Errors.JIRA_TASK_NOT_FOUND);
        }

        entity.setSelected(request.isSelected());
        entity = jiraTaskRepository.save(entity);
        return conversionService.convert(entity, JiraTaskDTO.class);
    }

    public List<JiraTaskDTO> getAllTask(String teamId) {

        return jiraTaskRepository.findByTeamId(teamId).stream()
                .map(entity -> conversionService.convert(entity, JiraTaskDTO.class))
                .collect(Collectors.toList());
    }

    public List<JiraTaskDTO> getAllSelectedTAsk(String teamId, Boolean selected) {
        return jiraTaskRepository.findByTeamIdAndSelectedEquals(teamId, selected).stream()
                .map(entity -> conversionService.convert(entity, JiraTaskDTO.class))
                .collect(Collectors.toList());
    }

    public JiraTaskDTO getTaskById(String id) {
        if(id == null) return null;
        return jiraTaskRepository.findById(id)
                .map(entity -> conversionService.convert(entity, JiraTaskDTO.class))
                .orElse(null);
    }

    public JiraTaskDTO createJiraTask(JiraTaskDTO jiraTask) {

        if (getTaskById(jiraTask.getTaskId()) != null) {
            throw new CustomException("Jira task taskId=" + jiraTask.getTaskId() + " already exists", Errors.JIRA_TASK_ALREADY_EXISTS);
        }

        JiraTaskEntity entity = JiraTaskEntity.builder()
                .taskId(generateTaskId())
                .teamId(jiraTask.getTeamId())
                .taskName(jiraTask.getTaskName())
                .description(jiraTask.getDescription())
                .type(jiraTask.getType())
                .taskStatus(jiraTask.getTaskStatus())
                .jiraUrl("http://jira.ru/" + jiraTask.getTaskId())
                .selected(false)
                .build();

        entity = jiraTaskRepository.save(entity);
        return conversionService.convert(entity, JiraTaskDTO.class);
    }

    private String generateTaskId() {
        int taskNumer = jiraTaskRepository.findAll().stream().map(jiraTaskEntity -> {
            return Integer.parseInt(jiraTaskEntity.getTaskId().split("-")[1]);
        }).max(Integer::compareTo).orElse(0);
        taskNumer++;
        return "GKR-" + taskNumer;
    }
}
