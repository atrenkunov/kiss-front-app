package ru.sber.kiss.task.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@Builder
@AllArgsConstructor
@NoArgsConstructor
public class JiraTaskDTO {

    private String taskId;
    private String teamId;
    private String taskName;
    private String description;
    private String type;
    private boolean selected;
    private String taskStatus;
    private String jiraUrl;

    public String getTaskId() {
        return taskId;
    }

    public String getTeamId() {
        return teamId;
    }

    public String getTaskName() {
        return taskName;
    }

    public String getDescription() {
        return description;
    }

    public String getType() {
        return type;
    }

    public boolean isSelected() {
        return selected;
    }

    public String getTaskStatus() {
        return taskStatus;
    }

    public String getJiraUrl() {
        return jiraUrl;
    }
}
