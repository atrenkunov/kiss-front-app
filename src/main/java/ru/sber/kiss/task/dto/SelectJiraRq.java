package ru.sber.kiss.task.dto;

public class SelectJiraRq {

    private String taskId;
    private boolean selected;

    public String getTaskId() {
        return taskId;
    }

    public boolean isSelected() {
        return selected;
    }

    @Override
    public String toString() {
        return "SelectJiraRq{" +
                "taskId=" + taskId +
                " selected=" + selected +
                '}';
    }
}
