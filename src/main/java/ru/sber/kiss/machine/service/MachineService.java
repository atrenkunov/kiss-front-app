package ru.sber.kiss.machine.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Component;
import ru.sber.kiss.error.CustomException;
import ru.sber.kiss.error.Errors;
import ru.sber.kiss.executor.core.Task;
import ru.sber.kiss.executor.service.ExecutorService;
import ru.sber.kiss.machine.dao.MachineRepository;
import ru.sber.kiss.machine.dao.entity.MachineEntity;
import ru.sber.kiss.machine.dto.InstallArtifactRq;
import ru.sber.kiss.machine.dto.MachineDTO;
import ru.sber.kiss.machine.dto.MachineSegment;

import javax.annotation.PostConstruct;
import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.util.*;
import java.util.stream.Collectors;

@Component
public class MachineService {

    private final Map<String, String> machinesInstallations = Collections.synchronizedMap(new HashMap<>());

    @Autowired
    private ExecutorService executorService;

    @Autowired
    private ConversionService conversionService;

    @Autowired
    private MachineRepository machineRepository;

    @PostConstruct
    public void init() {
        machineRepository.initMachines();
    }

    public MachineDTO getMachineById(String id) {
        return machineRepository.findById(id)
                .map(entity -> conversionService.convert(entity, MachineDTO.class))
                .map(dto -> {
                    dto.setInstallationProgress(getInstallationStatus(id));
                    return dto;
                })
                .orElse(null);
    }

    public void deleteMachine(String id) {
        if (getMachineById(id) == null) {
            throw new CustomException("Machine id=" + id + " not found", Errors.MACHINE_NOT_FOUND);
        }
        machineRepository.deleteById(id);
    }

    public MachineDTO createMachine(MachineDTO machine) {

        if (machineRepository.existsByIp(machine.getIp())) {
            throw new CustomException("Machine with ip=" + machine.getIp() + " already exists", Errors.MACHINE_ALREADY_EXISTS);
        }

        MachineEntity entity = MachineEntity.builder()
                .id(UUID.randomUUID().toString())
                .ip(machine.getIp())
                .type(machine.getType())
                .name(machine.getName())
                .teamId(machine.getTeamId())
                .build();

        entity = machineRepository.save(entity);
        return conversionService.convert(entity, MachineDTO.class);
    }

    public List<MachineDTO> getMachinesByTeam(String teamId, MachineSegment machineSegment) {

        return machineRepository.findByTeamIdAndType(teamId, machineSegment).stream()
                .map(entity -> conversionService.convert(entity, MachineDTO.class))
                .map(dto -> {
                    dto.setInstallationProgress(getInstallationStatus(dto.getId()));
                    return dto;
                })
                .collect(Collectors.toList());
    }

    public MachineDTO installArtifact(InstallArtifactRq request) {

        MachineEntity entity = machineRepository.findById(request.getMachineId()).orElse(null);
        if (entity == null) {
            throw new RuntimeException("Machine id=" + request.getMachineId() + " not found");
        }

        entity.setInstalledArtifactId(request.getArtifactId());
        entity.setDescription(request.getDescription());
        entity.setOwner(request.getOwner());

        machineRepository.save(entity);

        Task task = executorService.installArtifact();
        machinesInstallations.put(request.getMachineId(), task.getId());

        return getMachineById(request.getMachineId());
    }

    private Task getInstallationStatus(String machineId) {

        String installationId = machinesInstallations.get(machineId);
        Task task = executorService.getTaskStatus(installationId);
        return task;
    }
}
