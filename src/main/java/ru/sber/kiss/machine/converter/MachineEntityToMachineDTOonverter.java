package ru.sber.kiss.machine.converter;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import ru.sber.kiss.machine.dao.entity.MachineEntity;
import ru.sber.kiss.machine.dto.MachineDTO;

@Component
public class MachineEntityToMachineDTOonverter implements Converter<MachineEntity, MachineDTO> {

    @Override
    public MachineDTO convert(MachineEntity source) {
        return MachineDTO.builder()
                .id(source.getId())
                .ip(source.getIp())
                .name(source.getName())
                .type(source.getType())
                .teamId(source.getTeamId())
                .installedArtifactId(source.getInstalledArtifactId())
                .owner(source.getOwner())
                .description(source.getDescription())
                .build();
    }
}
