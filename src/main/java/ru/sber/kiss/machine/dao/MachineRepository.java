package ru.sber.kiss.machine.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import ru.sber.kiss.machine.dao.entity.MachineEntity;
import ru.sber.kiss.machine.dto.MachineSegment;

import javax.transaction.Transactional;
import java.util.List;

public interface MachineRepository extends JpaRepository<MachineEntity, String> {

    List<MachineEntity> findByTeamIdAndType(String teamId, MachineSegment type);

    @Transactional
    @Modifying
    @Query(value = "update machine set installed_artifact_id = NULL, owner = null, description = null", nativeQuery = true)
    void initMachines();

    boolean existsByIp(String ip);
}
