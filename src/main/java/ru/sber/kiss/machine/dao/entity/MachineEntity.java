package ru.sber.kiss.machine.dao.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import ru.sber.kiss.machine.dto.MachineSegment;

import javax.persistence.*;

@Entity
@Table(name = "machine")
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MachineEntity {

    @Id
    @Column(name = "id")
    private String id;

    @Column(name = "ip")
    private String ip;

    @Column(name = "type")
    @Enumerated(EnumType.STRING)
    private MachineSegment type;

    @Column(name = "name")
    private String name;

    @Column(name = "team_id")
    private String teamId;

    @Column(name = "installed_artifact_id")
    private String installedArtifactId;

    @Column(name = "owner")
    private String owner;

    @Column(name = "description")
    private String description;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getInstalledArtifactId() {
        return installedArtifactId;
    }

    public void setInstalledArtifactId(String installedArtifactId) {
        this.installedArtifactId = installedArtifactId;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public MachineSegment getType() {
        return type;
    }

    public void setType(MachineSegment type) {
        this.type = type;
    }

    public String getTeamId() {
        return teamId;
    }

    public void setTeamId(String teamId) {
        this.teamId = teamId;
    }
}
