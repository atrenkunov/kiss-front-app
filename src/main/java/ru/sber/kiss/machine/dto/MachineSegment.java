package ru.sber.kiss.machine.dto;

public enum MachineSegment {
    DEV,
    IFT,
    ST,
    PSI,
    PROM
}
