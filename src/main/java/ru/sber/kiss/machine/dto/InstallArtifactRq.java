package ru.sber.kiss.machine.dto;

public class InstallArtifactRq {

    private String artifactId;
    private String machineId;
    private String owner;
    private String description;

    public String getArtifactId() {
        return artifactId;
    }

    public String getMachineId() {
        return machineId;
    }

    public String getOwner() {
        return owner;
    }

    public String getDescription() {
        return description;
    }
}
