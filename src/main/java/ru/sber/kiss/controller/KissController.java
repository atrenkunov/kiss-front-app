package ru.sber.kiss.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.sber.kiss.auth.service.AuthService;
import ru.sber.kiss.task.dto.SelectJiraRq;
import ru.sber.kiss.dto.*;
import ru.sber.kiss.error.CustomException;
import ru.sber.kiss.executor.StageInfo;
import ru.sber.kiss.executor.core.Task;
import ru.sber.kiss.executor.dto.CreateBuildRq;
import ru.sber.kiss.executor.service.ExecutorService;
import ru.sber.kiss.integration.dao.entity.IntegrationEntity;
import ru.sber.kiss.integration.dao.entity.LogEntity;
import ru.sber.kiss.integration.service.IntegrationService;
import ru.sber.kiss.machine.dto.InstallArtifactRq;
import ru.sber.kiss.machine.dto.MachineDTO;
import ru.sber.kiss.machine.dto.MachineSegment;
import ru.sber.kiss.machine.service.MachineService;
import ru.sber.kiss.nexus.dto.ArtifactInfo;
import ru.sber.kiss.nexus.service.NexusService;
import ru.sber.kiss.repo.dto.BranchDTO;
import ru.sber.kiss.repo.dto.BranchInfoRq;
import ru.sber.kiss.repo.dto.CreateBranchRq;
import ru.sber.kiss.repo.dto.RepositoryDTO;
import ru.sber.kiss.repo.service.RepositoryService;
import ru.sber.kiss.task.dto.JiraTaskDTO;
import ru.sber.kiss.task.service.JiraTaskService;
import ru.sber.kiss.team.dto.TeamDTO;
import ru.sber.kiss.team.service.TeamService;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.time.OffsetDateTime;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Controller.
 */
@RestController
@RequestMapping(path = "api")
public class KissController {

    @Autowired
    private IntegrationService integrationService;

    @Autowired
    private TeamService teamService;

    @Autowired
    private ObjectMapper mapper;

    @Autowired
    private ConversionService conversionService;

    @Autowired
    private RepositoryService repositoryService;

    @Autowired
    private AuthService authService;

    @Autowired
    private ExecutorService executorService;

    @Autowired
    private JiraTaskService jiraTaskService;

    @Autowired
    private NexusService nexusService;

    @Autowired
    private MachineService machineService;

    @GetMapping("hello")
    public String hello() {
        return "Kiss controller is here: " + new Date();
    }

    @GetMapping("auth/info")
    public AuthorizationInfo getUserInfo() {
        return conversionService.convert(authService.getUserInfo(), AuthorizationInfo.class);
    }

    @GetMapping("repo/all/byTeam")
    public List<RepositoryShortInfo> getRepositoriesByTeam(@RequestParam("teamId") String teamId) {

        return repositoryService.getRepositories(teamId).stream()
                .map(dto -> conversionService.convert(dto, RepositoryShortInfo.class))
                .collect(Collectors.toList());
    }

    @PostMapping("repo/{repositoryId}/branch")
    public BranchInfo createBranch(@PathVariable("repositoryId") String repositoryId,
                                   @RequestBody CreateBranchRq request) {
        return conversionService.convert(repositoryService.createBranch(request), BranchInfo.class);
    }

    @GetMapping("repo/{repositoryId}")
    public RepositoryInfo getRepositoryInfo(@PathVariable("repositoryId") String repositoryId) {
        RepositoryDTO repositoryDTO = repositoryService.getRepository(repositoryId);


        List<BranchInfo> branchList = repositoryDTO.getBranchList().stream()
                .map(dto -> conversionService.convert(dto, BranchInfo.class))
                .map(branch -> {
                    Task lastBuild = getBranchLastBuild(branch.getRepositoryId(), branch.getId());
                    branch.setLastBuild(lastBuild);
                    return branch;
                })
                .collect(Collectors.toList());

        return RepositoryInfo.builder()
                .id(repositoryDTO.getId())
                .name(repositoryDTO.getName())
                .link(repositoryDTO.getLink())
                .branchList(branchList)
                .build();
    }

    @PostMapping("repo/{repoId}/branch/info")
    public BranchInfo getRepositoryInfo(@RequestBody BranchInfoRq branchInfoRq) {

        BranchDTO branch = repositoryService.getBranch(branchInfoRq.getRepositoryId(), branchInfoRq.getId());
        if (branch == null) {
            return null;
        }
        return conversionService.convert(branch, BranchInfo.class);
    }

    @GetMapping("executor/build/stage/all")
    public List<StageInfo> getBuildStages() {
        return executorService.getBuildStages();
    }

    @GetMapping("executor/install/stage/all")
    public List<StageInfo> getInstallStages() {
        return executorService.getInstallStages();
    }

    @GetMapping("executor/build/all/byTeam")
    private List<Task> getBuildsByTeam(@RequestParam("teamId") String teamId) {
        return executorService.getBuildsByTeam(teamId);
    }

    @PostMapping("executor/build")
    public Task buildBranch(@RequestBody CreateBuildRq request) {
        return executorService.buildBranch(request);
    }

    @GetMapping("executor/build/{buildId}/status")
    public Task getTaskStatus(@PathVariable("buildId") String buildId) {
        return executorService.getTaskStatus(buildId);
    }

    @GetMapping("executor/build/{buildId}/result")
    public ArtifactInfo getTaskResult(@PathVariable("buildId") String buildId) {
        return executorService.getTaskResult(buildId);
    }

    @GetMapping("jira/task/byTeamId")
    public List<JiraTaskInfo> getJiraTaskAll(@RequestParam("teamId") String teamId) {
        return jiraTaskService.getAllTask(teamId).stream()
                .map(this::convertJiraDTOToJiraInfo)
                .collect(Collectors.toList());
    }

    @GetMapping("jira/task/byTeamIdInWork")
    public List<JiraTaskInfo> getJiraTaskAllSelected(@RequestParam("teamId") String teamId,
                                                     @RequestParam("selected") Boolean selected) {
        return jiraTaskService.getAllSelectedTAsk(teamId, selected).stream()
                .map(this::convertJiraDTOToJiraInfo)
                .collect(Collectors.toList());
    }

    @GetMapping("jira/task/all")
    public List<JiraTaskInfo> getJiraTaskAll() {
        return jiraTaskService.getAllTask().stream()
                .map(this::convertJiraDTOToJiraInfo)
                .collect(Collectors.toList());
    }

    @PostMapping("jira/task/select")
    public JiraTaskInfo selectJiraTask(@RequestBody SelectJiraRq request) {
        return convertJiraDTOToJiraInfo(jiraTaskService.selectJiraTask(request));
    }

    @PostMapping("jira/task")
    public JiraTaskInfo createJiraTask(@RequestBody JiraTaskDTO jiraTask) {
        return convertJiraDTOToJiraInfo(jiraTaskService.createJiraTask(jiraTask));
    }

    @GetMapping("nexus/artifact/all/byTeam")
    public Collection<JiraTaskArtifacts> getArtifactsByTeam(@RequestParam("teamId") String teamId) {

        Map<String, JiraTaskArtifacts> jiraArtifacts = new HashMap<>();

        for (ArtifactInfo artifactInfo : nexusService.getArtifactsByTeam(teamId)) {

            String branchId = artifactInfo.getBuildInfo().getBranchId();
            String repoId = artifactInfo.getBuildInfo().getRepositoryId();

            JiraTaskDTO jiraTask = getJiraTaskIdByBranch(branchId);

            String jiraId = Objects.nonNull(jiraTask) ? jiraTask.getTaskId() : "";
            String jiraTaskName = Objects.nonNull(jiraTask) ? jiraTask.getTaskName() : "";

            jiraArtifacts.putIfAbsent(jiraId, new JiraTaskArtifacts(jiraId, jiraTaskName, new ArrayList<>()));

            BranchArtifacts branchArtifacts = getBranchArtifacts(jiraArtifacts.get(jiraId), repoId, branchId);
            branchArtifacts.getArtifacts().add(artifactInfo);
        }

        return jiraArtifacts.values();
    }

    private BranchArtifacts getBranchArtifacts(JiraTaskArtifacts jiraArtifacts, String repoId, String branchId) {

        BranchArtifacts branchArtifact = null;

        for (BranchArtifacts tmp : jiraArtifacts.getBranches()) {
            if (Objects.equals(repoId, tmp.getRepositoryId()) && Objects.equals(branchId, tmp.getBranchId())) {
                branchArtifact = tmp;
            }
        }

        if (branchArtifact == null) {
            branchArtifact = new BranchArtifacts(repoId, branchId, new ArrayList<>());
            jiraArtifacts.getBranches().add(branchArtifact);
        }

        return branchArtifact;
    }

    @PostMapping("machine/{machineId}/install")
    public MachineInfo installArtifact(@RequestBody InstallArtifactRq request) {

        MachineDTO dto = machineService.installArtifact(request);
        return convertMachineDTOToMachineInfo(dto);
    }

    @GetMapping("machine/{machineId}")
    public MachineInfo getMachineById(@PathVariable("machineId") String machineId) {

        MachineDTO dto = machineService.getMachineById(machineId);
        return convertMachineDTOToMachineInfo(dto);
    }

    @DeleteMapping("machine/{machineId}")
    public void deleteMachineById(@PathVariable("machineId") String id) {
        machineService.deleteMachine(id);
    }

    @PostMapping("machine")
    public MachineInfo createMachine(@RequestBody MachineDTO machine) {

        MachineDTO dto = machineService.createMachine(machine);
        return convertMachineDTOToMachineInfo(dto);
    }

    @GetMapping("machine/{machineSegment}/byTeam")
    public List<MachineInfo> getMachinesByTeam(@PathVariable("machineSegment") String machineSegment,
                                               @RequestParam("teamId") String teamId) {

        List<MachineDTO> machines = machineService.getMachinesByTeam(teamId, MachineSegment.valueOf(machineSegment));
        return machines.stream()
                .map(this::convertMachineDTOToMachineInfo)
                .collect(Collectors.toList());
    }

    @GetMapping("team/{teamId}")
    public TeamDTO getTeam(@PathVariable("teamId") String teamId) {
        return teamService.findTeam(teamId);
    }

    @GetMapping("team/all")
    public List<TeamDTO> getAllTeams() {
        return teamService.getAll();
    }

    @PostMapping("integration")
    public IntegrationEntity createIntegration(@RequestBody IntegrationEntity integration) {
        return integrationService.createIntegration(integration);
    }

    @GetMapping("integration/{integrationId}")
    public IntegrationEntity getIntegration(@PathVariable("integrationId") Integer integrationId) {
        return integrationService.getIntegration(integrationId);
    }

    @GetMapping("integration/integrationTypes")
    public List<String> getintegrationTypes() {
        return integrationService.getIntegrationTypes();
    }

    @DeleteMapping("integration/{integrationId}")
    public void deleteIntegration(@PathVariable("integrationId") Integer integrationId) {
        integrationService.deleteIntegration(integrationId);
    }

    @GetMapping("integration/all/byTeam={teamId}")
    public List<IntegrationEntity> getAllIntegrationsByTeamId(@PathVariable("teamId") String teamId) {
        return integrationService.getAllIntegrationsByTeamId(teamId);
    }

    @GetMapping("integration/{integrationId}/log")
    public List<LogEntity> getIntegrationLog(@PathVariable("integrationId") String integrationId) {
        return integrationService.getIntegrationLog(integrationId);
    }

    @PostMapping("integration/{integrationId}/set/{status}")
    public String setIntegrationStatus(@PathVariable("integrationId") Integer integrationId,
                                       @PathVariable("status") String status) {
        return integrationService.setIntegrationStatus(integrationId, status);
    }

    private Task makeDeepTaskCopy(Task task) {
        try {
            return mapper.readValue(mapper.writeValueAsBytes(task), Task.class);
        } catch (IOException ignored) {
            return null;
        }
    }

    private JiraTaskDTO getJiraTaskIdByBranch(String branchId) {
        String jiraTaskId;

        int pos = branchId.lastIndexOf("/");
        if (pos < 0) {
            jiraTaskId = branchId;
        } else {
            jiraTaskId = branchId.substring(pos + 1);
        }
        return jiraTaskService.getTaskById(jiraTaskId);
    }

    @ExceptionHandler
    public ResponseEntity<Map> exceptionHandler(HttpServletRequest request, Exception ex) {

        Map<String, Object> response = new HashMap<>();

        response.put("timestamp", OffsetDateTime.now());
        response.put("status", HttpStatus.INTERNAL_SERVER_ERROR.value());
        response.put("error", ex.toString());
        response.put("stacktrace", getStackTrace(ex.getStackTrace()));

        ex.printStackTrace();

        return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler
    public ResponseEntity<Map> customExceptionHandler(HttpServletRequest request, CustomException ex) {

        Map<String, Object> response = new HashMap<>();

        response.put("timestamp", OffsetDateTime.now());
        response.put("status", HttpStatus.INTERNAL_SERVER_ERROR.value());
        response.put("error", ex.toString());
        response.put("stacktrace", getStackTrace(ex.getStackTrace()));
        response.put("code", ex.getCode());

        ex.printStackTrace();

        return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    private List<String> getStackTrace(StackTraceElement[] stackTrace) {

        if (stackTrace == null) {
            return new ArrayList<>();
        }

        return Arrays.stream(stackTrace)
                .map(StackTraceElement::toString)
                .collect(Collectors.toList());
    }

    private Task getBranchLastBuild(String repositoryId, String branchId) {

        Task lastBuild = executorService.getLastBranchBuild(repositoryId, branchId);
        if (lastBuild != null) {
            lastBuild = makeDeepTaskCopy(lastBuild);
            lastBuild.setId(executorService.getTaskResult(lastBuild.getId()).getName());
            lastBuild.getStages().removeIf(stage -> !executorService.getQualityGates().contains(stage.getId()));
        }
        return lastBuild;
    }

    private MachineInfo convertMachineDTOToMachineInfo(MachineDTO dto) {

        ArtifactInfo artifact = nexusService.get(dto.getInstalledArtifactId());

        JiraTaskDTO jiraTask = null;
        if (Objects.nonNull(artifact)) {
            JiraTaskDTO jiraTaskInfo = getJiraTaskIdByBranch(artifact.getBuildInfo().getBranchId());
            if (Objects.nonNull(jiraTaskInfo)) {
                String jiraTaskId = jiraTaskInfo.getTaskId();
                jiraTask = jiraTaskService.getTaskById(jiraTaskId);
            }
        }

        return MachineInfo.builder()
                .id(dto.getId())
                .ip(dto.getIp())
                .name(dto.getName())
                .type(dto.getType().name())
                .owner(dto.getOwner())
                .jiraTask(jiraTask)
                .description(dto.getDescription())
                .installedArtifact(artifact)
                .installationProgress(dto.getInstallationProgress())
                .build();
    }

    public JiraTaskInfo convertJiraDTOToJiraInfo(JiraTaskDTO dto) {

        BranchInfo branchInfo = repositoryService.getBranchesWithIdLike(dto.getTaskId()).stream().findFirst()
                .map(branch -> {
                    BranchInfo info = conversionService.convert(branch, BranchInfo.class);
                    Task lastBuild = getBranchLastBuild(branch.getRepositoryId(), branch.getId());
                    info.setLastBuild(lastBuild);
                    return info;
                })
                .orElse(null);

        return JiraTaskInfo.builder()
                .taskId(dto.getTaskId())
                .teamId(dto.getTeamId())
                .taskName(dto.getTaskName())
                .description(dto.getDescription())
                .type(dto.getType())
                .selected(dto.isSelected())
                .taskStatus(dto.getTaskStatus())
                .jiraUrl(dto.getJiraUrl())
                .branch(branchInfo)
                .build();
    }
}