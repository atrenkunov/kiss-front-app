package ru.sber.kiss.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import ru.sber.kiss.task.dto.JiraTaskDTO;
import ru.sber.kiss.task.service.JiraTaskService;

@RestController
@RequestMapping(path = "view")
public class ViewController {

    @Autowired
    private JiraTaskService jiraTaskService;

    @GetMapping("confluence/sigma/{taskId}")
    public ModelAndView getSigmaConfluence(@PathVariable("taskId") String taskId) {

        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("jiraTaskId", taskId);

        JiraTaskDTO jiraTask = jiraTaskService.getTaskById(taskId);
        if (jiraTask == null) {
            modelAndView.setViewName("noInfo");
        } else {
            modelAndView.setViewName("sigmaConfluence");
            modelAndView.addObject("jiraTaskName", jiraTask.getTaskName());
            modelAndView.addObject("jiraTaskDescription", jiraTask.getDescription());
        }
        return modelAndView;
    }

    @GetMapping("confluence/alpha/{taskId}")
    public ModelAndView getAlphaConfluence(@PathVariable("taskId") String taskId) {

        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("jiraTaskId", taskId);

        JiraTaskDTO jiraTask = jiraTaskService.getTaskById(taskId);
        if (jiraTask == null) {
            modelAndView.setViewName("noInfo");
        } else {
            modelAndView.setViewName("alphaConfluence");
            modelAndView.addObject("jiraTaskName", jiraTask.getTaskName());
            modelAndView.addObject("jiraTaskDescription", jiraTask.getDescription());
        }
        return modelAndView;
    }
}
