package ru.sber.kiss.repo.service;

import ru.sber.kiss.repo.dto.BranchDTO;
import ru.sber.kiss.repo.dto.CreateBranchRq;
import ru.sber.kiss.repo.dto.RepositoryDTO;
import ru.sber.kiss.repo.dto.RepositoryShortDTO;

import java.util.List;

public interface RepositoryService {

    List<RepositoryShortDTO> getRepositories(String teamId);

    BranchDTO createBranch(CreateBranchRq request);

    RepositoryDTO getRepository(String repositoryId);

    BranchDTO getBranch(String repositoryId, String branchId);

    List<BranchDTO> getBranchesWithIdLike(String name);
}
