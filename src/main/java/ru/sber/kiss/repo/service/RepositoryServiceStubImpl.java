package ru.sber.kiss.repo.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Component;
import ru.sber.kiss.error.CustomException;
import ru.sber.kiss.error.Errors;
import ru.sber.kiss.repo.dao.RepoBranchRepository;
import ru.sber.kiss.repo.dao.RepoRepository;
import ru.sber.kiss.repo.dao.entity.RepositoryBranchEntity;
import ru.sber.kiss.repo.dao.entity.RepositoryEntity;
import ru.sber.kiss.repo.dto.BranchDTO;
import ru.sber.kiss.repo.dto.CreateBranchRq;
import ru.sber.kiss.repo.dto.RepositoryDTO;
import ru.sber.kiss.repo.dto.RepositoryShortDTO;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Component
@ConditionalOnProperty(value = "kiss.stub.repo", havingValue = "true")
public class RepositoryServiceStubImpl implements RepositoryService {

    @Autowired
    private ConversionService conversionService;

    @Autowired
    private RepoBranchRepository branchRepository;

    @Autowired
    private RepoRepository repoRepository;

    @Override
    public List<RepositoryShortDTO> getRepositories(String teamId) {

        return repoRepository.findByTeamId(teamId).stream()
                .map(entity -> conversionService.convert(entity, RepositoryShortDTO.class))
                .collect(Collectors.toList());
    }

    @Override
    public BranchDTO createBranch(CreateBranchRq request) {
        String repositoryId = request.getRepositoryId();
        String sourceBranchId = request.getSourceId();

        RepositoryBranchEntity entity = branchRepository.findByRepositoryIdAndBranchId(repositoryId, sourceBranchId).orElse(null);
        if (entity == null) {
            throw  new CustomException("Branch repositoryId=" + repositoryId + " branchId=" + sourceBranchId + " not found", Errors.BRANCH_NOT_FOUND);
        }

        validateBranchAlreadyExists(request.getId());

        RepositoryBranchEntity newEntity = new RepositoryBranchEntity();
        newEntity.setRepositoryId(repositoryId);
        newEntity.setBranchId(request.getId());
        newEntity.setLastCommit(entity.getLastCommit());
        newEntity.setModuleVersion(entity.getModuleVersion());
        return conversionService.convert(branchRepository.save(newEntity), BranchDTO.class);
    }

    private void validateBranchAlreadyExists(String branchId) {

        List<BranchDTO> branches = getBranchesWithIdLike(getJiraTaskIdByBranch(branchId));
        if (!branches.isEmpty()) {
            throw new CustomException(String.format("There are similarly branches like %s e.g. %s", branchId, branches), Errors.BRANCH_ALREADY_EXISTS);
        }
    }

    @Override
    public RepositoryDTO getRepository(String repositoryId) {

        RepositoryEntity repositoryEntity = repoRepository.findById(repositoryId).orElse(null);
        if (repositoryEntity == null) {
            throw new CustomException("Repository id=" + repositoryId + " not found", Errors.REPOSITORY_NOT_FOUND);
        }

        RepositoryDTO info = new RepositoryDTO();
        info.setId(repositoryEntity.getId());
        info.setName(repositoryEntity.getName());
        info.setLink(repositoryEntity.getLink());

        branchRepository.findByRepositoryId(repositoryId).stream()
                .map(entity -> conversionService.convert(entity, BranchDTO.class))
                .forEach(branch -> {
                    info.getBranchList().add(branch);
                });

        return info;
    }

    @Override
    public BranchDTO getBranch(String repositoryId, String branchId) {
        return branchRepository.findByRepositoryIdAndBranchId(repositoryId, branchId)
                .map(entity -> conversionService.convert(entity, BranchDTO.class))
                .orElse(null);
    }

    @Override
    public List<BranchDTO> getBranchesWithIdLike(String name) {

        return branchRepository.findAllByBranchIdContainsIgnoreCase(name).stream()
                .map(entity -> conversionService.convert(entity, BranchDTO.class))
                .collect(Collectors.toList());
    }

    @PostConstruct
    public void init() {
        log.info("RepositoryServiceStubImpl is here");
    }

    private String getJiraTaskIdByBranch(String branchId) {
        String jiraTaskId;

        int pos = branchId.lastIndexOf("/");
        if (pos < 0) {
            jiraTaskId = branchId;
        } else {
            jiraTaskId = branchId.substring(pos + 1);
        }
        return jiraTaskId;
    }
}
