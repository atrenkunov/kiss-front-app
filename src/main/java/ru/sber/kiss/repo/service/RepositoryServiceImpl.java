package ru.sber.kiss.repo.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;
import ru.sber.kiss.repo.dto.BranchDTO;
import ru.sber.kiss.repo.dto.CreateBranchRq;
import ru.sber.kiss.repo.dto.RepositoryDTO;
import ru.sber.kiss.repo.dto.RepositoryShortDTO;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@Component
@ConditionalOnProperty(value = "kiss.stub.repo", havingValue = "false")
public class RepositoryServiceImpl implements RepositoryService {

    @Override
    public List<RepositoryShortDTO> getRepositories(String teamId) {
        return new ArrayList<>();
    }

    @Override
    public BranchDTO createBranch(CreateBranchRq request) {
        return null;
    }

    @Override
    public RepositoryDTO getRepository(String repositoryId) {
        return null;
    }

    @Override
    public BranchDTO getBranch(String repositoryId, String branchId) {
        return null;
    }

    @Override
    public List<BranchDTO> getBranchesWithIdLike(String name) {
        return null;
    }

    @PostConstruct
    public void init() {
        log.info("RepositoryServiceImpl is here");
    }
}
