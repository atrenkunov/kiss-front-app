package ru.sber.kiss.repo.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.sber.kiss.repo.dao.entity.RepositoryEntity;

import java.util.List;

public interface RepoRepository extends JpaRepository<RepositoryEntity, String> {

    List<RepositoryEntity> findByTeamId(String teamId);
}