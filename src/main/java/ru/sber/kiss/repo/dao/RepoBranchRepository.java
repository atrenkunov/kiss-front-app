package ru.sber.kiss.repo.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.sber.kiss.repo.dao.entity.RepositoryBranchEntity;

import java.util.List;
import java.util.Optional;

public interface RepoBranchRepository extends JpaRepository<RepositoryBranchEntity, String> {

    List<RepositoryBranchEntity> findByRepositoryId(String repositoryId);

    Optional<RepositoryBranchEntity> findByRepositoryIdAndBranchId(String repositoryId, String branchId);

    List<RepositoryBranchEntity> findAllByBranchIdContainsIgnoreCase(String name);
}