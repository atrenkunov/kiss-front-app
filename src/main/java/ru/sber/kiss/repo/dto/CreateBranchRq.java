package ru.sber.kiss.repo.dto;

/**
 * Request for branch creation.
 */
public class CreateBranchRq {

    private String repositoryId;
    private String id;
    private String sourceId;

    public String getRepositoryId() {
        return repositoryId;
    }

    public String getId() {
        return id;
    }

    public String getSourceId() {
        return sourceId;
    }
}
