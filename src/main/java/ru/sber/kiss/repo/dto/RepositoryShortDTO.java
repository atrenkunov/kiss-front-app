package ru.sber.kiss.repo.dto;

public class RepositoryShortDTO {

    private String id;
    private String name;
    private String link;

    public RepositoryShortDTO(String id, String name, String link) {
        this.id = id;
        this.name = name;
        this.link = link;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getLink() {
        return link;
    }

    @Override
    public String toString() {
        return "RepositoryShortDTO{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", link='" + link + '\'' +
                '}';
    }
}
