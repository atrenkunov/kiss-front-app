package ru.sber.kiss.repo.dto;

import java.util.ArrayList;
import java.util.List;

public class RepositoryDTO {

    private String id;
    private String name;
    private String link;
    private List<BranchDTO> branchList;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public List<BranchDTO> getBranchList() {
        if (branchList == null) {
            branchList = new ArrayList<>();
        }
        return branchList;
    }

    public void setBranchList(List<BranchDTO> branchList) {
        this.branchList = branchList;
    }
}
