package ru.sber.kiss.repo.dto;

public class BranchInfoRq {

    private String repositoryId;
    private String id;

    public BranchInfoRq() {
    }

    public BranchInfoRq(String repositoryId, String id) {
        this.repositoryId = repositoryId;
        this.id = id;
    }

    public String getRepositoryId() {
        return repositoryId;
    }

    public String getId() {
        return id;
    }

}
