package ru.sber.kiss.repo.dto;

import lombok.Builder;

@Builder
public class BranchDTO {

    private String repositoryId;
    private String id;
    private String lastCommit;
    private String moduleVersion;

    public String getRepositoryId() {
        return repositoryId;
    }

    public void setRepositoryId(String repositoryId) {
        this.repositoryId = repositoryId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLastCommit() {
        return lastCommit;
    }

    public void setLastCommit(String lastCommit) {
        this.lastCommit = lastCommit;
    }

    public String getModuleVersion() {
        return moduleVersion;
    }

    public void setModuleVersion(String moduleVersion) {
        this.moduleVersion = moduleVersion;
    }

    @Override
    public String toString() {
        return "BranchDTO{" +
                "repositoryId='" + repositoryId + '\'' +
                ", id='" + id + '\'' +
                ", lastCommit='" + lastCommit + '\'' +
                ", moduleVersion='" + moduleVersion + '\'' +
                '}';
    }
}
