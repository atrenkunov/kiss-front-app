package ru.sber.kiss.repo.converter;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import ru.sber.kiss.dto.RepositoryShortInfo;
import ru.sber.kiss.repo.dao.entity.RepositoryEntity;
import ru.sber.kiss.repo.dto.RepositoryShortDTO;

@Component
public class RepositoryEntityToRepositoryShortDTOConverter implements Converter<RepositoryEntity, RepositoryShortDTO> {

    @Override
    public RepositoryShortDTO convert(RepositoryEntity entity) {
        return new RepositoryShortDTO(entity.getId(), entity.getName(), entity.getLink());
    }
}
