package ru.sber.kiss.repo.converter;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import ru.sber.kiss.repo.dao.entity.RepositoryBranchEntity;
import ru.sber.kiss.repo.dto.BranchDTO;

@Component
public class RepositoryBranchEntityToBranchDTOConverter implements Converter<RepositoryBranchEntity, BranchDTO> {

    @Override
    public BranchDTO convert(RepositoryBranchEntity entity) {

        return BranchDTO.builder()
                .id(entity.getBranchId())
                .repositoryId(entity.getRepositoryId())
                .lastCommit(entity.getLastCommit())
                .moduleVersion(entity.getModuleVersion())
                .build();
    }
}
