package ru.sber.kiss.nexus.service;

import org.springframework.stereotype.Component;
import ru.sber.kiss.nexus.dto.ArtifactInfo;

import java.util.*;

@Component
public class NexusService {

    private Map<String, List<ArtifactInfo>> artifactsByTeam = new HashMap<>();
    private Map<String, ArtifactInfo> artifacts = new HashMap<>();

    public synchronized ArtifactInfo put(String teamId, ArtifactInfo artifactInfo) {

        String artifactId = artifactInfo.getId();

        artifacts.put(artifactId, artifactInfo);
        ArtifactInfo artifact = artifacts.get(artifactId);
        artifactsByTeam.compute(teamId, (k, old) -> {
            if (old == null) {
                old = new ArrayList<>();
            }
            old.add(artifact);
            return old;
        });
        return artifactInfo;
    }

    public List<ArtifactInfo> getArtifactsByTeam(String teamId) {
        return artifactsByTeam.getOrDefault(teamId, new ArrayList<>());
    }

    public ArtifactInfo get(String artifactId) {
        return artifacts.get(artifactId);
    }
}
