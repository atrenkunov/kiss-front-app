package ru.sber.kiss.nexus.dto;

import ru.sber.kiss.executor.core.Task;

import java.time.OffsetDateTime;
import java.util.UUID;

public class ArtifactInfo {

    private String id;
    private String name;
    private String link;
    private final OffsetDateTime timestamp;
    private Task buildInfo;

    public ArtifactInfo() {
        timestamp = OffsetDateTime.now();
    }

    public ArtifactInfo(String name, Task buildInfo) {
        this();
        this.id = UUID.randomUUID().toString();
        this.name = name;
        this.buildInfo = buildInfo;
        this.link = "http://nexus.ru/builds/" + name + "/summary";
    }

    public String getId() {
        return id;
    }

    public String getLink() {
        return link;
    }

    public OffsetDateTime getTimestamp() {
        return timestamp;
    }

    public String getName() {
        return name;
    }

    public Task getBuildInfo() {
        return buildInfo;
    }
}
