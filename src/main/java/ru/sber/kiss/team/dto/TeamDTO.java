package ru.sber.kiss.team.dto;

import lombok.Builder;

/**
 * Command info.
 */
@Builder
public class TeamDTO {

    private String id;
    private String name;
    private String owner;
    private String ownerContact;
    private String support;
    private String supportContact;

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getOwner() {
        return owner;
    }

    public String getOwnerContact() {
        return ownerContact;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public void setOwnerContact(String ownerContact) {
        this.ownerContact = ownerContact;
    }

    public String getSupport() {
        return support;
    }

    public void setSupport(String support) {
        this.support = support;
    }

    public String getSupportContact() {
        return supportContact;
    }

    public void setSupportContact(String supportContact) {
        this.supportContact = supportContact;
    }

    @Override
    public String toString() {
        return "TeamDTO{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", owner='" + owner + '\'' +
                ", ownerContact='" + ownerContact + '\'' +
                ", support='" + support + '\'' +
                ", supportContact='" + supportContact + '\'' +
                '}';
    }
}
