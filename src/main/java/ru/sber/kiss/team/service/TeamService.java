package ru.sber.kiss.team.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Component;
import ru.sber.kiss.team.dto.TeamDTO;
import ru.sber.kiss.team.dao.TeamRepository;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class TeamService {

    @Autowired
    private ConversionService conversionService;

    @Autowired
    private TeamRepository teamRepository;

    public TeamDTO findTeam(String teamId) {

        return teamRepository.findById(teamId)
                .map(entity -> conversionService.convert(entity, TeamDTO.class))
                .orElse(null);
    }

    public List<TeamDTO> getAll() {

        return teamRepository.findAll().stream()
                .map(entity -> conversionService.convert(entity, TeamDTO.class))
                .collect(Collectors.toList());
    }
}
