package ru.sber.kiss.team.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.sber.kiss.team.dao.entity.TeamEntity;

public interface TeamRepository extends JpaRepository<TeamEntity, String> {
}
