package ru.sber.kiss.team.dao.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Command entity.
 */
@Entity
@Table(name = "team")
public class TeamEntity {

    @Id
    @Column(name = "id")
    private String id;

    @Column(name = "name")
    private String name;

    @Column(name = "owner")
    private String owner;

    @Column(name = "owner_contact")
    private String ownerContact;

    @Column(name = "support")
    private String support;

    @Column(name = "support_contact")
    private String supportContact;

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getOwner() {
        return owner;
    }

    public String getOwnerContact() {
        return ownerContact;
    }

    public String getSupport() {
        return support;
    }

    public String getSupportContact() {
        return supportContact;
    }
}
